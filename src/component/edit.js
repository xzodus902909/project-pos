import { Component } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import { UpdateBox } from "../service/box";
export default class edit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      email: "",
      gender: "",
    };
  }
  getname = (e) => {
    this.setState({ name: e.target.value });
  };
  getemail = (e) => {
    this.setState({ email: e.target.value });
  };
  getgender = (e) => {
    this.setState({ gender: e.target.value });
  };
  onsubmit = (e) => {
    e.preventDefault();
    const dataBox={
      "name": this.state.name,
      "email": this.state.email,
      "gender": this.state.gender,
      
  }
  UpdateBox(this.props.match.params.id,dataBox);
    console.log("ok");
    console.log(`name is ${this.state.name}`);
    console.log(`name is ${this.state.email}`);
    console.log(`name is ${this.state.gender}`);
    this.setState({ name: "", email: "", gender: "" });
  };
  render() {
    return (
      <div className="form-wrapper mt-5">
        <h1>post data</h1>
        <Form onSubmit={this.onsubmit}>
          <Form.Group>
            <Form.Label>Name</Form.Label>
            <Form.Control type="text" value={this.state.name} onChange={this.getname}></Form.Control>
          </Form.Group>
          <Form.Group>
            <Form.Label>Email</Form.Label>
            <Form.Control type="text" value={this.state.email} onChange={this.getemail}></Form.Control>
          </Form.Group>
          <Form.Group>
            <Form.Label>gender</Form.Label>
            <Form.Control type="text" value={this.state.gender} onChange={this.getgender}></Form.Control>
          </Form.Group>
          <Button size="lg" variant="success"  block="block" type="submit" > 
            Submit
          </Button>
        </Form>
      </div>
    );
  }
}
